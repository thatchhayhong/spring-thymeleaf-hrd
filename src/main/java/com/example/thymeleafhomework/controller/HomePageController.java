package com.example.thymeleafhomework.controller;
import com.example.thymeleafhomework.model.ArticleModel;
import com.example.thymeleafhomework.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HomePageController {
   private  final ArticleService articleService;
    @Autowired
    public HomePageController(ArticleService articleService) {
        this.articleService = articleService;
    }
    @GetMapping("")
    public  String pathEmtpy(){
        return "redirect:/home";
    }

    @GetMapping("/home")
    public  String home(Model model){
        model.addAttribute("articles",articleService.findAll());
        return "/index";
    }
    @GetMapping("/home?lang=us")
    public  String homeEng(){
        return "redirect:/home?lang=us";
    }
    @GetMapping("/home?lang=kh")
    public  String homeKh(){
        return "redirect:/home?lang=kh";
    }
    @GetMapping("/home?lang=kr")
    public  String homeKr(){
        return "redirect:/home?lang=kr";
    }

    @PostMapping("/create/add")
    public String articleAdd(@ModelAttribute ArticleModel articleModel){
            List<ArticleModel> articleModelList = articleService.findAll();
            articleModel.setId(articleModelList.size()+1);
            articleService.addNew(articleModel);
            return "redirect:/home";
    }

    @GetMapping("/create")
    public String create() {
            return "fragements/form-fragement";
        }

    @GetMapping("/update/")
    public String update(@RequestParam int id,Model model) {
        model.addAttribute("article",articleService.findOne(id));
        return "fragements/update-fragement";
    }
    @PostMapping("/update/")
    public String updateArticle(@RequestParam int id,@ModelAttribute ArticleModel articleModel){
        articleService.update(id,articleModel);
        return "redirect:/home";
    }
    @GetMapping("/view/")
    public String viewByID(@RequestParam int id,Model model){
        model.addAttribute("viewArticle",articleService.findOne(id));
        return "fragements/detail-article";
    }
    @GetMapping("/delete/")
    public String deleteByID(@RequestParam int id){
        articleService.delete(id);
        return "redirect:/home";
    }
}
