package com.example.thymeleafhomework.repository;

import com.example.thymeleafhomework.model.ArticleModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ArticleRepository {
    List<ArticleModel>articleModelList=new ArrayList<>();
    {
        articleModelList.add(new ArticleModel(1,"HRD ReactJS","Front End","images/reactjs.jpg"));
        articleModelList.add(new ArticleModel(2,"HRD Spring","Backend Web Service","images/spring.jpg"));
    }
    public List<ArticleModel>findAll(){
        return articleModelList;
    }
    public ArticleModel findOne(int id){
        ArticleModel articleModel = articleModelList.get(id-1);
        return articleModel;
    }
    public void  addNew(ArticleModel articleModel){
        articleModelList.add(articleModel);
    }
    public  void delete(int id){
        articleModelList.remove(id-1);
    }
    public void update(int id,ArticleModel articleModel){
        articleModelList.set(id-1,articleModel);
    }
}
