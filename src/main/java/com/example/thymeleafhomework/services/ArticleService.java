package com.example.thymeleafhomework.services;

import com.example.thymeleafhomework.model.ArticleModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public interface ArticleService {
    List<ArticleModel> findAll();
    public  ArticleModel findOne(int id);
    public void addNew(ArticleModel articleModel);
    public void delete(int id);
    public void update(int id,ArticleModel articleModel);
}
