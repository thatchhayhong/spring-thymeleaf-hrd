package com.example.thymeleafhomework.services.imp;

import com.example.thymeleafhomework.model.ArticleModel;
import com.example.thymeleafhomework.repository.ArticleRepository;
import com.example.thymeleafhomework.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {
    private final ArticleRepository articleRepository;
    @Autowired
    public ArticleServiceImp(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<ArticleModel> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public ArticleModel findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public void addNew(ArticleModel articleModel) {
    articleRepository.addNew(articleModel);
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

    @Override
    public void update(int id, ArticleModel articleModel) {
        articleRepository.update(id,articleModel);
    }
}
